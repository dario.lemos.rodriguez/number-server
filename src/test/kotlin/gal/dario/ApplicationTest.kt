package gal.dario

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.OutputStreamWriter
import java.io.PrintWriter
import java.lang.Thread.sleep
import java.net.Socket
import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong
import kotlin.io.path.absolutePathString

class ApplicationTest {

    private val startingNumber = 100000000

    @TempDir
    private lateinit var tempDir: Path

    @Test
    fun `IT max load test`() {

        val path = tempDir.resolve("numbers-test.log")
        val application = Application(fileName = path.absolutePathString())
        val executorService = Executors.newSingleThreadExecutor()
        executorService.submit { application.runServer() }
        val size = AtomicLong(0L)
        sleep(500)
        val futures = mutableListOf<Future<*>>()
        val socket = Socket("127.0.0.1", Application.PORT)
        val clientExecutorService = Executors.newSingleThreadExecutor()
        for (j in 1..10) {
            futures.add(clientExecutorService.submit { createClientCall(socket, j) })
        }

        clientExecutorService.awaitTermination(5, TimeUnit.SECONDS)

        size.addAndGet(Files.lines(path).count())
        path.toFile().deleteOnExit()

        assertEquals(10000, size.get())
    }

    private fun createClientCall(socket: Socket, i: Int) {
        val printWriter = PrintWriter(OutputStreamWriter(socket.getOutputStream()))
        val beginning = startingNumber + (i * 1000)
        val end = beginning + 1000 - 1
        for (n in beginning..end) {
            printWriter.println(n.toString())
        }
        printWriter.flush()
    }

}
