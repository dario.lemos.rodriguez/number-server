package gal.dario.client

import gal.dario.processor.LineReader
import org.junit.jupiter.api.BeforeEach
import org.mockito.Mock
import java.net.Socket

class ClientTest {
    @Mock
    private lateinit var mockClientSocket: Socket
    @Mock
    private lateinit var lineReader: LineReader
    private lateinit var clientToTest: Client

    @BeforeEach
    fun setup() {
        clientToTest = ClientFactory(lineReader).client()
    }
}