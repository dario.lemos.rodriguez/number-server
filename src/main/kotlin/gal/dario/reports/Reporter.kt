package gal.dario.reports

import gal.dario.processor.NumberProcessor

class Reporter(private val processor: NumberProcessor) : Runnable {
    override fun run() {
        processor.getReport().sendReport()
    }
}