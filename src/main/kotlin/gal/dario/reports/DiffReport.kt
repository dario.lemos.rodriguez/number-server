package gal.dario.reports

data class DiffReport(val totalUniques: Int, val duplicatesInRound: Int, val uniquesInRound: Int) {
    fun sendReport() {
        println("Received $uniquesInRound unique numbers, $duplicatesInRound duplicates. Unique total: $totalUniques")
    }
}