package gal.dario.server

import gal.dario.client.Client
import gal.dario.client.ClientFactory
import gal.dario.client.ClientResult
import org.slf4j.LoggerFactory
import java.io.Closeable
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

class Server private constructor(
    port: Int,
    private val maxClients: Int,
    private val clientFactory: ClientFactory
) : Runnable, Closeable {
    private val isRunning = AtomicBoolean(true)
    private val logger = LoggerFactory.getLogger(Server::class.java)

    private val serverSocket by lazy { ServerSocket(port, maxClients) }
    private val currentClients by lazy { ConcurrentHashMap<Client, Socket>(maxClients) }

    private fun closeClientSocket(socket: Socket) = socket.close()

    private fun acceptConnection(socket: Socket) {
        val client = clientFactory.client()
        client.use {
            currentClients[it] = socket

            val clientResult = it.process(socket)

            currentClients.remove(it)
            when (clientResult) {
                ClientResult.FORCED_TERMINATION -> shutdown()
                ClientResult.INVALID_INPUT, ClientResult.ERROR_READING, ClientResult.READ_EXHAUSTED -> {
                    socket.close()
                }
                ClientResult.CLIENT_DISCONNECTED -> {}
            }
        }
    }

    private fun shutdown() {
        isRunning.set(false)
        logger.trace("Starting server shutdown....")
        currentClients.forEach {
            it.key.close()
            it.value.close()
        }
        currentClients.clear()

        serverSocket.close()
        logger.trace("Server shutdown completed.")
    }

    override fun close() = shutdown()

    companion object {
        fun getInstance(
            port: Int,
            maxClients: Int,
            clientFactory: ClientFactory
        ) = Server(port, maxClients, clientFactory)
    }

    override fun run() {
        while (isRunning.get()) {
            try {
                val socket = serverSocket.accept()
                when {
                    currentClients.size < maxClients -> thread { acceptConnection(socket)  }
                    else -> closeClientSocket(socket)
                }
            } catch (e: Exception) {
                when {
                    e is SocketException && serverSocket.isClosed -> logger.trace("Socket was closed with message: ${e.message}")
                    else -> logger.error("Could not accept new connection", e)
                }
            }
        }
    }
}

