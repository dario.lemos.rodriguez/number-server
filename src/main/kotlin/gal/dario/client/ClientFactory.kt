package gal.dario.client

import gal.dario.processor.LineReader

class ClientFactory(private val lineReader: LineReader): AutoCloseable {
    fun client(): Client {
        return Client(lineReader)
    }

    override fun close() {
        lineReader
    }
}