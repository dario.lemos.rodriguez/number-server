package gal.dario.client

import gal.dario.processor.LineReader
import java.io.Closeable
import java.net.Socket
import java.util.Scanner

class Client(private val lineReader: LineReader) : AutoCloseable, Closeable {

    @Volatile
    private var currentState = ClientState.STARTED

    fun process(socket: Socket): ClientResult {
        currentState = ClientState.RUNNING
        if (socket.isClosed) {
            return ClientResult.CLIENT_DISCONNECTED
        }

        val inputStream = socket.getInputStream()

        return inputStream.use {
            return@use try {
                lineReader.read(Scanner(inputStream))
            } catch (e: Exception) {
                ClientResult.ERROR_READING
            }
        }
    }

    override fun close() {
        currentState = ClientState.CLOSED
    }
}

enum class ClientResult {
    CLIENT_DISCONNECTED,
    READ_EXHAUSTED,
    INVALID_INPUT,
    FORCED_TERMINATION,
    ERROR_READING
}

enum class ClientState {
    STARTED,
    RUNNING,
    CLOSED
}
