package gal.dario

import gal.dario.client.ClientFactory
import gal.dario.processor.LineReader
import gal.dario.processor.NumberProcessor
import gal.dario.reports.Reporter
import gal.dario.server.Server
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.Closeable
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread

class Application(
    private val fileName: String = FILE_NAME,
    private val port: Int = PORT,
    private val maxClients: Int = MAX_CLIENTS,
    private val reportScheduleInterval: Long = REPORT_SCHEDULE_INTERVAL,
    private val reportScheduleIntervalUnit: TimeUnit = TimeUnit.SECONDS
) : AutoCloseable, Closeable {
    private val serverExecutor = Executors.newSingleThreadExecutor()
    private val scheduledExecutor = Executors.newSingleThreadScheduledExecutor()

    fun runServer() {
        val processor = NumberProcessor(fileName)
        val reporter = Reporter(processor)
        scheduledExecutor.scheduleAtFixedRate(reporter, 0, reportScheduleInterval, reportScheduleIntervalUnit)

        thread {
            val lineReader = LineReader(processor)
            val clientFactory = ClientFactory(lineReader)
            val server = Server.getInstance(port, maxClients, clientFactory)
            runBlocking {
                launch {
                    server.run()
                }
            }.invokeOnCompletion {
                server.close()
            }
        }
    }

    companion object {
        const val PORT = 4000
        const val MAX_CLIENTS = 5
        const val FILE_NAME = "numbers.log"
        const val REPORT_SCHEDULE_INTERVAL = 10L
    }

    override fun close() {
        scheduledExecutor.shutdown()
        serverExecutor.shutdown()
    }
}

fun main(args: Array<String>) {
    Application().apply { runServer() }
}
