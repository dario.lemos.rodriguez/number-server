package gal.dario.processor

import gal.dario.client.ClientResult
import java.util.Scanner

class LineReader(private val processor: NumberProcessor) {
    fun read(scanner: Scanner): ClientResult {
        while (scanner.hasNextLine()) {
            val line = scanner.nextLine()

            return when (process(line)) {
                LineResult.INVALID_INPUT -> ClientResult.INVALID_INPUT
                LineResult.TERMINATE -> ClientResult.FORCED_TERMINATION
                LineResult.CONTINUE -> continue
            }
        }

        return ClientResult.READ_EXHAUSTED
    }

    private fun process(line: String): LineResult {
        return if (line.length != VALID_LINE_LENGTH) {
            LineResult.INVALID_INPUT
        } else if (TERMINATION_TOKEN == line) {
            LineResult.TERMINATE
        } else {
            processValidNumber(line)
            LineResult.CONTINUE
        }
    }

    private fun processValidNumber(line: String) = processor.process(line.toInt())

    companion object {
        const val VALID_LINE_LENGTH = 9
        const val TERMINATION_TOKEN = "terminate"
    }
}

enum class LineResult {
    CONTINUE,
    INVALID_INPUT,
    TERMINATE
}
