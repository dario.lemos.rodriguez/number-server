package gal.dario.processor

import gal.dario.reports.DiffReport
import java.io.Closeable
import java.io.FileWriter
import java.nio.file.Files
import java.nio.file.Paths
import java.util.BitSet
import java.util.Timer
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.fixedRateTimer
import kotlin.io.path.absolutePathString

class NumberProcessor(pathToLogFile: String) : AutoCloseable, Closeable {
    private val uniqueNumbers = BitSet(999999999)
    private val totalUniqueNumbersCount = AtomicInteger(0)

    private val uniqueNumbersInRound = AtomicInteger(0)
    private val duplicateNumbersInRound = AtomicInteger(0)

    private val path = Paths.get(pathToLogFile)

    private val fileWriter: FileWriter
    private val fixedRateTimer: Timer

    init {
        Files.deleteIfExists(path)
        fileWriter = FileWriter(path.absolutePathString(), true)
        fixedRateTimer = fixedRateTimer(period = 1000) {
            fileWriter.flush()
        }
    }

    private fun reportUniqueNumber(uniqueNumber: String) {
        fileWriter.write(uniqueNumber + System.lineSeparator())
    }

    @Synchronized
    fun process(element: Int): Boolean {
        return if (uniqueNumbers.get(element)) {
            duplicateNumbersInRound.incrementAndGet()

            false
        } else {
            uniqueNumbers.set(element)
            uniqueNumbersInRound.incrementAndGet()
            reportUniqueNumber(element.toString())

            true
        }
    }

    @Synchronized
    fun getReport(): DiffReport {
        fileWriter.flush()
        val uniqueNumbersToReport = uniqueNumbersInRound.getAndSet(0)
        val duplicateNumbersToReport = duplicateNumbersInRound.getAndSet(0)
        val totalUniqueNumbersToReport = totalUniqueNumbersCount.addAndGet(uniqueNumbersToReport)

        return DiffReport(totalUniqueNumbersToReport, duplicateNumbersToReport, uniqueNumbersToReport)
    }

    override fun close() {
        fixedRateTimer.cancel()
        fileWriter.flush()
        fileWriter.close()
    }
}